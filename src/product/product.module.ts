import { Product, ProductSchema } from './schema/Product';

import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductController } from './product.controller';
import { ProductService } from './product.service';

@Module({
  imports : [MongooseModule.forFeature([{name: Product.name, schema : ProductSchema }]),
  MongooseModule.forRoot('mongodb://localhost:27017/admin')],
  controllers: [ProductController],
  providers: [ProductService]
})
export class ProductModule {}
