import { Module } from '@nestjs/common';
import { BookService } from './book.service';
import { BookController } from './book.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { BookSchema } from './schema/Book';
import { Book } from './entities/book.entity';

@Module({
  imports : [MongooseModule.forFeature([{name: Book.name, schema : BookSchema }]),
             MongooseModule.forRoot('mongodb://localhost:27017/admin')],
  controllers: [BookController],
  providers: [BookService]
})
export class BookModule {}
